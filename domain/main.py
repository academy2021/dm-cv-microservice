import pandas as pd
import numpy as np
import logging
import glob
import time
import cv2
import os
from src.pdf_to_image import pdf_to_jpg

from src.utils import read_image_from_buffer, time_usage, save_image, save_txt, build_log_txt
from src.region_detection import region_detection, merged_regions
from src.text_extraction import text_extraction
from src.visualize import draw_labeled_boxes, show_bounding_boxes, save_classification_results
from src.boxes_classification import apply_classification
from src.postprocessing import make_json, get_label_name
from src.benchmark import dilatation_benchmark
from src.make_dataset import make_input_buffer_sample

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

# TODO: remove all functions from main
# TODO: use the same logger for all functions
def pdf_to_json(
    buffer_image, dilatation_iterations=5, output_name="", zoom=1,
    with_merge=False, model_path="domain/models/best_classification_model",
    tfidf_model_path="domain/models/tfidfconverter", svd_model_path="domain/models/svd",
    competences_dataset_path="./domain/datasets/external_datasets/competences.csv",
    log_folder=""):
    """
    Loads a CV image from the given path, parse
    the CV and gather information into a JSON object.
    """

    #TODO: Refactor
    # Add pdf to image
    start_time = time.time()
    
    image = read_image_from_buffer(buffer_image, zoom=zoom)
    regions = region_detection(image, iterations=dilatation_iterations, log_folder=log_folder)
    
    if with_merge:
        regions = merged_regions(regions)

    boxes = text_extraction(image, regions, log_folder=log_folder)

    labeled_boxes = apply_classification(
        boxes, model_path=model_path, tfidf_model_path=tfidf_model_path,
        svd_model_path=svd_model_path, log_folder=log_folder)

    json_res = make_json(labeled_boxes, competences_dataset_path, output_name, log_folder=log_folder)
    if len(log_folder):
        save_classification_results(image=image, log_folder=log_folder, dilatation_itr=dilatation_iterations)
        seconds_elapsed = time.time() - start_time
        content = build_log_txt(buffer_image, dilatation_iterations, output_name, with_merge, log_folder,
                                image, regions, boxes, labeled_boxes, json_res, seconds_elapsed)
        save_txt(content, log_folder + "/log_summary.txt")

    return json_res


#Deprecated
def pdf_parsing(pdf_path):
    """
    Loads a CV image from the given path, parse
    the CV and gather information into a pd.DataFrame.
    """

    # Add pdf to image
    image = read_image(pdf_path)
    regions = region_detection(image)
    boxes = text_extraction(image, regions)
    labeled_boxes = apply_classification(boxes)

    return labeled_boxes


def pdf_parsing_benchmark_sample(pdf_folder_path="datasets/dataset zoom 3/*.jpg", number_samples=50, output_path="output/pdf_parsing_benchmark_on_samples.csv"):
    """
    """

    cv_file_paths = pd.read_csv("output/benchmark.csv")["cv_name"].map(lambda x: "datasets/dataset zoom 3/"+x+".jpg").unique()
    #FIXME
    seen_cvs = pd.read_csv(output_path, encoding="ISO-8859-1")["cv_name"].map(lambda x: "datasets/dataset zoom 3/"+x+".jpg")
    cv_file_paths = list(set(cv_file_paths) - set(seen_cvs))
    
    benchmark_df = pd.DataFrame({
        "cv_name": [],
        "text": [],
        "read_image_ET": [],
        "region_detection_ET": [],
        "text_extraction_ET": [],
        "labeled_boxes_ET": [],
        "make_json_ET": [],
        "nombre_boxes": [],
        "total_execution_time": []})
    for file_path in cv_file_paths:
        #file_path = cv_file_paths[file_idx]
        logging.info(50*"*")
        #cv_name = file_path.split("\\")[1][:-4]
        cv_name = file_path.split("/")[2].split(".")[0]
        #print(file_path.split("/"))
        #logging.info(f"Parsing CV: {cv_name}")
        benchmark_row = {}
        with time_usage("Parsing d'un CV"):
            benchmark_row["cv_name"] = cv_name
            total_ts = time.time()

            ts = time.time()
            image = read_image(file_path)
            execution_time = time.time() - ts
            benchmark_row["read_image_ET"] = execution_time

            ts = time.time()
            regions = region_detection(image)
            execution_time = time.time() - ts
            benchmark_row["region_detection_ET"] = execution_time

            ts = time.time()
            boxes = text_extraction(image, regions)
            execution_time = time.time() - ts
            benchmark_row["text_extraction_ET"] = execution_time

            ts = time.time()
            labeled_boxes = apply_classification(boxes)
            execution_time = time.time() - ts
            benchmark_row["labeled_boxes_ET"] = execution_time

            ts = time.time()
            json_dict = make_json(boxes_dataframe=labeled_boxes, json_file_name=f"output/json_dump/{cv_name}.json")
            execution_time = time.time() - ts
            benchmark_row["make_json_ET"] = execution_time

            total_execution_time = time.time() - total_ts
            benchmark_row["total_execution_time"] = total_execution_time

            benchmark_row["nombre_boxes"] = len(boxes)
            for label in json_dict:
                benchmark_row["label"] = label
                benchmark_row["text"] = json_dict[label]
                benchmark_df = benchmark_df.append(
                    benchmark_row, ignore_index=True)

        # Saving for safety..
        with open(output_path, 'a') as f:
            benchmark_df.to_csv(f, index=False, mode='a', header=not f.tell())


def pdf_parsing_batch(pdf_folder_path="datasets/dataset zoom 3/*.jpg"):
    """
    """

    benchmark_df = pd.DataFrame(
        {"cv_name": [], "text": [], "label": [], "execution_time": []})
    parsed_cvs_list = glob.glob("json_dump/*.json")
    logging.info("Parsed CVs list")
    logging.info(parsed_cvs_list)
    for file_path in glob.glob(pdf_folder_path):
        logging.info(50*"*")
        cv_name = file_path.split("\\")[1][:-4]
        logging.info(f'Parsing CV: {cv_name}')
        if f"json_dump\\{cv_name}.json" in parsed_cvs_list:
            logging.info(f'CV: {cv_name} skipped')
            continue
        benchmark_row = {}
        glob.glob("json_dump")
        with time_usage("Parsing d'un CV"):
            benchmark_row["cv_name"] = cv_name
            ts = time.time()
            labeled_boxes = pdf_parsing(file_path)
            json_dict = make_json(labeled_boxes, f"json_dump/{cv_name}.json")
            execution_time = time.time() - ts
            benchmark_row["execution_time"] = execution_time
            for label in json_dict:
                benchmark_row["label"] = label
                benchmark_row["text"] = json_dict[label]
                benchmark_df = benchmark_df.append(
                    benchmark_row, ignore_index=True)

        # Saving for safety..
        benchmark_df.to_csv("benchmark.csv", index=False)


# TEST ZONE
# pdf_parsing_benchmark_sample()
"""
pdf_to_json("datasets/dataset zoom 3/HAMMOUZOU_Sara.jpg", output_name="output/exemple5.json")
pdf_to_json("datasets/dataset zoom 3/ADNANE_20Maryam.jpg", output_name="output/exemple1.json")
pdf_to_json("datasets/dataset zoom 3/Younes_EL_IDRISSI_YAZAMI.jpg", output_name="output/exemple2.json")
pdf_to_json("datasets/dataset zoom 3/Younes_BAHIAOUI.jpg", output_name="output/exemple3.json")
pdf_to_json("datasets/dataset zoom 3/LMOUTAOUAKIL_Mohamed.jpg", output_name="output/exemple4.json")
pdf_to_json("datasets/dataset zoom 3/HAMMOUZOU_Sara.jpg", output_name="output/exemplewith_merge5.json", with_merge=True)
pdf_to_json("datasets/dataset zoom 3/ADNANE_20Maryam.jpg", output_name="output/exemplewith_merge1.json", with_merge=True)
pdf_to_json("datasets/dataset zoom 3/Younes_EL_IDRISSI_YAZAMI.jpg", output_name="output/exemplewith_merge2.json", with_merge=True)
pdf_to_json("datasets/dataset zoom 3/Younes_BAHIAOUI.jpg", output_name="output/exemplewith_merge3.json", with_merge=True)
pdf_to_json("datasets/dataset zoom 3/LMOUTAOUAKIL_Mohamed.jpg", output_name="output/exemplewith_merge4.json", with_merge=True)
"""
# pdf_parsing_benchmark_sample(number_samples=100)
#draw_labeled_boxes("datasets/dataset zoom 3/ADNANE_20Maryam.jpg", "output/exemple2.jpg", extra_text="zoom_3_dilatation_4")

#image = read_image("datasets/dataset zoom 3/Younes_EL_IDRISSI_YAZAMI.jpg")
# print(type(image))
# print(image.shape)
##image_a = read_image("datasets/dataset zoom 3/Younes_EL_IDRISSI_YAZAMI.jpg")
#image_b = read_image("datasets/dataset zoom 3/Younes_EL_IDRISSI_YAZAMI.jpg")

#regions = region_detection(image)
# print(len(regions))
# print(regions)
#merged_regions_var = merged_regions(regions, distance_x_max=150, distance_y_max=99999)
# print(merged_regions)
# print(len(merged_regions_var))
#double_merged_regions = merged_regions(merged_regions_var, distance_x_max=50, distance_y_max=99999)
# print(merged_regions)
# print(len(double_merged_regions))
#show_bounding_boxes(image, regions, image_name="output/regions_Younes_EL_IDRISSI_YAZAMI.jpg")
#show_bounding_boxes(image_a, merged_regions_var, image_name="output/merged_regions_Younes_EL_IDRISSI_YAZAMI.jpg")
#show_bounding_boxes(image_b, double_merged_regions, image_name="output/double_merged_regions_Younes_EL_IDRISSI_YAZAMI.jpg")
#dilatation_benchmark("datasets/dataset zoom 3/ADNANE_20Maryam.jpg")

#CV_NAME = "MGHARI_20Ali-Hachim"
#pdf_to_json(
#    dilatation_iterations=5,
#    with_merge=True,
#    pdf_path=f"datasets/dataset zoom 3/{CV_NAME}.jpg",
#    output_name=f"output/{CV_NAME}/{CV_NAME}.json",
#    log_folder=f"output/{CV_NAME}")

#pdf_parsing_benchmark_sample(output_path="output/benchmark_dilatation_5.csv")

import sys


if __name__ == "__main__":

    #pdf_to_jpg("domain/datasets/pdf/MGHARI%20Ali-Hachim.pdf", zoom_factor=1)
    #FIXME
    
    CV_NAME = "input_example"
    
    #buffer_image = sys.argv[1].encode()
    buffer_image = make_input_buffer_sample("domain/datasets/input_example.jpg")

    json_res = pdf_to_json(
        dilatation_iterations=5,
        with_merge=True,
        buffer_image=buffer_image,
        output_name=f"output/{CV_NAME}/{CV_NAME}.json",
        log_folder=f"output/{CV_NAME}")
