import logging
import cv2
import os
from src.utils import read_image, save_image, save_dict

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def region_detection(image, iterations=5, log_folder=""):
    """
    Takes into parameters a np.array() image, add return
    a dictionnary containing regions and their coordinates
    """

    logging.info('Performaing image pre-processing...')
    logging.info('\t Gray scaling image')
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    logging.info('\t Applying gaussian blur')
    blur = cv2.GaussianBlur(gray, (7, 7), 0)
    logging.info('\t Binarizing image')
    thresh = cv2.threshold(
        blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    logging.info('Create rectangular structuring element and dilate...')
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    logging.info(f'\t Applying {iterations} dilatation iterations')
    dilate = cv2.dilate(thresh, kernel, iterations=iterations)

    logging.info('Looking for regions...')
    cnts = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    logging.info('Building regions dict...')
    regions = {}
    for i, c in enumerate(cnts):
        x, y, w, h = cv2.boundingRect(c)
        regions[f"box_{i+1}"] = {
            "xmin": x,
            "ymin": y,
            "xmax": x+w,
            "ymax": y+h,
        }
    
    #TODO: refactor
    if len(log_folder) > 0:
        log_region_detection_files(image, gray, blur, thresh, dilate, iterations, regions, log_folder)

    return regions


def merged_regions(regions, distance_x_max=300, distance_y_max=0):
    """
    """

    def is_close(region_a, region_b, distance_x_max=distance_x_max, distance_y_max=distance_y_max):
        return ((abs(max(region_a["xmax"], region_b["xmax"]) - 
               (region_a["xmax"] - region_a["xmin"]) -
               (region_b["xmax"] - region_b["xmin"]) -
               min(region_a["xmin"], region_b["xmin"])) < distance_x_max) or 
               (abs(max(region_a["ymax"], region_b["ymax"]) - 
               (region_a["ymax"] - region_a["ymin"]) -
               (region_b["ymax"] - region_b["ymin"]) -
               min(region_a["ymin"], region_b["ymin"])) < distance_y_max))
        
    regions_candidates = regions.copy()
    seen_regions = []
    merged_regions = {}
    for region in regions.values():
        merged = False
        if region not in seen_regions:
            for candidate_region in regions_candidates.values():
                if candidate_region not in seen_regions and candidate_region != region:
                    if is_close(region, candidate_region):
                        xmin = min((region["xmin"], candidate_region["xmin"]))
                        ymin = min((region["ymin"], candidate_region["ymin"]))
                        xmax = max((region["xmax"], candidate_region["xmax"]))
                        ymax = max((region["ymax"], candidate_region["ymax"]))
                        merged_regions[f"box_{len(merged_regions) + 1}"] = {
                            "xmin": xmin,
                            "ymin": ymin,
                            "xmax": xmax,
                            "ymax": ymax,
                        }

                        seen_regions.append(candidate_region)
                        merged = True
                    break
            if not merged:
                merged_regions[f"box_{len(merged_regions) + 1}"] = {**region}
        seen_regions.append(region)
        
    return merged_regions


def save_image_with_regions(image, boxes, image_name="output/show_bounding_boxes", extra_text=""):
    """
    Draws bounding bounding boxes on the given image,
    and saves it into the given folder. 
    """

    for _, region in boxes.items():
        x_min = int(region["xmin"])
        x_max = int(region["xmax"])
        y_min = int(region["ymin"])
        y_max = int(region["ymax"])
        
        image = cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (36,255,12), 2)
        image = cv2.putText(image, extra_text, (20, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    
    cv2.imwrite(f"{image_name}.png", image)


def log_region_detection_files(
    image, gray, blur, thresh, dilate, dilatation_itr, regions, log_folder):
    """
    Saves all intermediate files into a log_folder.
    """

    
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)

    save_image(log_folder + "/gray.jpg", gray)
    save_image(log_folder + "/blur.jpg", blur)
    save_image(log_folder + "/thresh.jpg", thresh)
    save_image(log_folder + "/dilate.jpg", dilate)
    save_dict(regions, log_folder + "/regions.json")
    save_image_with_regions(
        image, regions, image_name=log_folder + "/image_with_all_boxes",
        extra_text=f"dilatation_iterations_{dilatation_itr}__nbre_regions_{len(regions)}")

    