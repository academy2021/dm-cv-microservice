import os

import re
import glob
import fitz
import logging
import requests
import pandas as pd
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
from urllib.request import Request, urlopen
"""
from preprocessing import clean_boxes_dataset
from utils import read_image
from region_detection import region_detection
from text_extraction import text_extraction

"""
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

def get_link(url):
    """Scrapes the content of an URL page and returns links contained
    in that page.
    Used to extract a list of links from https://www.emi.ac.ma/emiste .
    """

    req = Request(url)
    html_page = urlopen(req)
    soup = BeautifulSoup(html_page, "html")

    links = []
    for link in soup.findAll('a'):
        link = link.get('href')
        if "pdf" in link or "doc" in link:
            links.append(link)

    return links


def download_file(base_url, link):
    """Requests the given link and saves its content
    into datasets folder.
    """

    file_link = base_url + "/" + link
    r = requests.get(file_link)
    open("datasets/" + link, 'wb').write(r.content)


def download_EMI_CVs():
    """Used to download CVs from EMI's website."""

    os.mkdir("datasets")
    for i in range(13, 20):
        url = f'https://www.emi.ac.ma/emiste/CVs_Ing_EMI_20{i}/Genie_Informatique/'
        links = get_link(url)
        print(f"20{i}")
        for link in links:
            print("\t" + link)
            download_file(url, link)


def pdf_to_jpg(pdf_file_path, zoom_factor=3, output_path="datasets/images/"):
    """Converts a pdf into one or multiple jpg images."""
    
    doc = fitz.open(pdf_file_path)
    mat = fitz.Matrix(zoom_factor, zoom_factor)

    for i in range(0, doc.pageCount):
        page = doc.loadPage(i)
        pix = page.getPixmap(matrix=mat)
        if doc.pageCount > 1:
            output = output_path + pdf_file_path[:-4] + f"{i}.jpg"
        else:
            output = output_path + pdf_file_path[:-4] + ".jpg"
        pix.writePNG(output)


def convert_EMI_CVs(pdf_folder_path="datasets/1/"):
    """Converts every pdf in pdf_folder and saves resulting images.
    """

    for file in glob.glob(f"{pdf_folder_path}*.pdf"):
        print(file)
        pdf_to_jpg(file)


def make_annotation_text_file(output_file_path="datasets/boxes_dataset.csv"):
    """
    """

    data = pd.DataFrame({
        "cv_name": [],
        "box_name": [],
        "xmin": [],
        "ymin": [],
        "xmax": [],
        "ymax": [],
        "text": []
    })

    for pdf_path in glob.glob("datasets/dataset zoom 3/*.jpg"):
        logging.info(f'CV name: {pdf_path}')
        image = read_image(pdf_path)
        regions = region_detection(image)
        boxes = text_extraction(image, regions)
        for box_name, box in boxes.items():
            data = data.append({
                "cv_name": pdf_path,
                "box_name": box_name,
                "xmin": box["xmin"],
                "ymin": box["ymin"],
                "xmax": box["xmax"],
                "ymax": box["ymax"],
                "text": box["text"],
            }, ignore_index=True)

    data.to_csv(output_file_path, index=False)


def make_annotation_dataset(boxes_dataset_path):
    """
    Creates an annotation dataset from boxes dataset
    (cleaned version).
    """

    data = pd.read_csv(boxes_dataset_path)

    special_string = "________"
    #FIXME: refactor
    annotation_data = data["cv_name"].map(lambda x: str(
        x)) + special_string + data["text"].map(lambda x: str(x))

    text_file = open("annotation_data.txt", "w", encoding="utf-8")

    annotation_text = "\n".join(annotation_data.values)

    text_file.write(annotation_text)
    text_file.close()


def apply_offset_labels(annotation_dataset_path, offset=9):
    """
    Apply a negative offset on labels id on doccano's output.
    """

    data = pd.read_csv(annotation_dataset_path)
    data["label"] = data["label"] - offset

    data.to_csv(
        "datasets/annotation dataset/annotation_oussama.csv", index=False)


def merge_annoted_datasets():
    """
    """

    root_path = "datasets/annotation dataset/annotation_"
    dataset_paths = ["chaymaa.csv", "malak.csv",
                     "yahiathen.csv", "oussama.csv"]

    combined_data = pd.concat([pd.read_csv(root_path + path)
                               for path in dataset_paths])
    annotated_data = combined_data.drop(
        ["user", "annotation_approver", "id"], axis=1)

    annotated_data.to_csv(
        root_path + "merged_annotated_dataset.csv", index=False)


def split_annotated_text(annotation_dataset_path):
    """
    """

    data = pd.read_csv(annotation_dataset_path)
    data[["cv_name", "text"]] = data["text"].str.split("________", expand=True)

    data.to_csv(annotation_dataset_path[:-4] + "_splited.csv", index=False)


def make_dataset():
    """
    """

    data_boxes = pd.read_csv("datasets/cleaned_boxes_dataset.csv")
    data_labeled = pd.read_csv(
        "datasets/annotation dataset/annotation_merged_annotated_dataset_splited.csv")

    data = pd.merge(left=data_boxes, right=data_labeled,
                    on=["text", "cv_name"], how="right")

    data.to_csv("datasets/classification_dataset.csv", index=False)


def make_input_buffer_sample(sample_image):
    """
    """

    buffer_image = open(sample_image, "rb").read()

    return buffer_image



"""
cleaned_data = clean_boxes_dataset("boxes_dataset.csv")
cleaned_data.to_csv("cleaned_boxes_dataset.csv", index=False)
make_annotation_dataset("cleaned_boxes_dataset.csv")
"""

# FIXME: refactor and add main()
#apply_offset_labels("datasets/annotation dataset/annotation_oussama_with_offset.csv")
#merge_annoted_datasets()
#make_dataset()
# FIXME: add main..
#split_annotated_text("datasets/annotation dataset/annotation_merged_annotated_dataset.csv")

#make_input_buffer_sample()