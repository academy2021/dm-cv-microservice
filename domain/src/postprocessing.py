import pandas as pd
import json
from src.utils import save_dict
from io import StringIO
from collections import Counter
import en_core_web_sm
from spacy.matcher import PhraseMatcher
from spacy.lang.en import English
import numpy as np
from unidecode import unidecode
import nltk
import re

nlp = en_core_web_sm.load()


LABEL_DICT = {
    1: "Infos_personnelles", 
    2: "Formations", 
    3: "Experiences", 
    4: "Langues", 
    5: "Projets", 
    6: "Certifications", 
    7: "Competences", 
    8: "Loisirs", 
    9: "Profil", 
    10: "Description", 
    11: "Autres"
}

JSON_OBJ = {
    "Infos_personnelles": "", 
    "Formations": "",
    "Experiences": "",
    "Langues": "",
    "Projets": "",
    "Certifications": "",
    "Competences": "",
    "Loisirs": "",
    "Profil": "",
    "Description": "",
    "Autres": ""
}

PROJECT_BANNED_SENTS = ["projets realises", "projets réalisés", "projets réalises", "projets réalisés",
            "projets", "réalisés", "réalises", "realisés", "réalisation", "projets académiques", 
            "projets academiques", "projets académiques réalisés", "académiques", "academiques",
            "projet realises"]

FORMATION_BANNED_SENTS = ["education", "formation", "educations", "formations", "dipl6mes et formation", 
                          "diplomes et formations", "dipl6mes et formation"]

LOISIRS_DICT = {
        "Sport": [ "football", "natation", "musculation", "Course à pied", "tennis", "boxe", "dance", "handball", "volleyball", "basketball"],
        "Culture": ["mangas", "musique", "cinema", "musee", "voyage"],
        "Autre": ["Bénevolat", "paramilitaire"]
    }

EXPERIENCES_BANNED_SENTS = ["expériences professionnelles", "expériences", "expérience professionnelle", "stage et projets", 
                            "stages et projets", "stage et projet", "stages et projet"]

LANGUES = ["francais", "arabe", "allemand", "espagnol", "amazigh", "chinois", "japonais", "berbere", "kabyle","anglais","frangais"]
NIVEAUX = ["native","c1","bonne","débutante","professionnelle", "debutante", "excellent","maternelle","bilingue","avance",
           "elementaire","moyen","natif", "bon", "courant", "debutant","maitrise","maitrise","intermediaire", "notions", "notion",
           "completes","complete","complètes", "complète","debutente"]


def merge_text_by_label(boxes_dataframes, character="\n"):
    """
    Merges classificaiton results by label using a special
    character.
    """

    return boxes_dataframes.groupby('label')['text'].apply(lambda x: '\n'.join(x)).reset_index()


def get_label_name(label_id):
    """
    """

    return LABEL_DICT[int(label_id)]


def extract_projects(text):
    """
    Extracts specific portions of the given text and returns
    a well defined dictionnary containing projects' details.
    """
    
    # Empty text
    if len(text) < 1:
        return {}
    
    projects_dict = {}
    projects_list = text.split("\n")
    for project in projects_list:
        project_words = project.split(" ")
        # Project has at most two words and is one of the banned sentences  
        if any(banned_sentence in project for banned_sentence in PROJECT_BANNED_SENTS) and len(project_words) < 3:
            continue
        # Project has one word
        if len(project_words) < 2:
            continue

        projects_dict[f"project_{len(projects_dict) + 1}"] = project

    return projects_dict


def extract_competences(text, competences_dataset_path="datasets/external datasets/competences.csv"):
    if len(text) < 1:
        return {}
    

    text = text.replace("\\n", " ")
    keyword_dict = pd.read_csv(competences_dataset_path)

    stats_words = [nlp(text) for text in keyword_dict['Statistics'].dropna(axis = 0)]
    NLP_words = [nlp(text) for text in keyword_dict['NLP'].dropna(axis = 0)]
    ML_words = [nlp(text) for text in keyword_dict['Machine Learning'].dropna(axis = 0)]
    DL_words = [nlp(text) for text in keyword_dict['Deep Learning'].dropna(axis = 0)]
    R_words = [nlp(text) for text in keyword_dict['R Language'].dropna(axis = 0)]
    python_words = [nlp(text) for text in keyword_dict['Python Language'].dropna(axis = 0)]
    Developement_words = [nlp(text) for text in keyword_dict['Developement'].dropna(axis = 0)]
    SGDB_words = [nlp(text) for text in keyword_dict['SGDB'].dropna(axis = 0)]
    Data_Engineering_words = [nlp(text) for text in keyword_dict['Data Engineering'].dropna(axis = 0)]
    Framework_words = [nlp(text) for text in keyword_dict['Framework'].dropna(axis = 0)]
    Patterns_words = [nlp(text) for text in keyword_dict['Patterns'].dropna(axis = 0)]
    Architecture_words = [nlp(text) for text in keyword_dict['Architecture'].dropna(axis = 0)]
    Cache_words = [nlp(text) for text in keyword_dict['Cache'].dropna(axis = 0)]
    Devops_words = [nlp(text) for text in keyword_dict['Devops'].dropna(axis = 0)]
    Cloud_words = [nlp(text) for text in keyword_dict['Cloud'].dropna(axis = 0)]
    Esb_words = [nlp(text) for text in keyword_dict['Esb'].dropna(axis = 0)]
    Systeme_words = [nlp(text) for text in keyword_dict['Systeme'].dropna(axis = 0)]
    Edi_words = [nlp(text) for text in keyword_dict['Edi'].dropna(axis = 0)]
    Organisation_words = [nlp(text) for text in keyword_dict['Organisation'].dropna(axis = 0)]
    Monitoring_words = [nlp(text) for text in keyword_dict['Monitoring'].dropna(axis = 0)]

    matcher = PhraseMatcher(nlp.vocab) 
    matcher.add('Stats', None, *stats_words)
    matcher.add('NLP', None, *NLP_words)
    matcher.add('ML', None, *ML_words)
    matcher.add('DL', None, *DL_words)
    matcher.add('R', None, *R_words)
    matcher.add('Python', None, *python_words)
    matcher.add('Dev', None, * Developement_words)
    matcher.add('SGDB', None, *SGDB_words)
    matcher.add('DE', None, *Data_Engineering_words)
    matcher.add('Framework', None, *Framework_words)
    matcher.add('Patterns', None, *Patterns_words)
    matcher.add('Architecture', None, *Architecture_words)
    matcher.add('Cache', None, *Cache_words)
    matcher.add('Devops', None, *Devops_words)
    matcher.add('Cloud', None, *Cloud_words)
    matcher.add('Esb', None, *Esb_words)
    matcher.add('Systeme', None, *Systeme_words)
    matcher.add('Edi', None, *Edi_words)
    matcher.add('Organisation', None, *Organisation_words)
    matcher.add('Monitoring', None, *Monitoring_words)

    doc = nlp(text)
 
    d = []  
    matches = matcher(doc)  
    for match_id, start, end in matches:
        rule_id = nlp.vocab.strings[match_id] 
        span = doc[start : end]  
        d.append((rule_id, span.text)) 
    keywords = "\n".join(f'{i[0]} {i[1]} ({j})' for i,j in Counter(d).items())

    df = pd.read_csv(StringIO(keywords),names = ['Keywords_List'])   
    df1 = pd.DataFrame(df.Keywords_List.str.split(' ',1).tolist(),columns = ['Subject','Keyword'])
    df2 = pd.DataFrame(df1.Keyword.str.split('(',1).tolist(),columns = ['Keyword', 'Count'])    
    df3 = pd.concat([df1['Subject'],df2['Keyword'], df2['Count']], axis =1)    
    df3['Count'] = df3['Count'].apply(lambda x: x.rstrip(")"))
  
    dataf = pd.concat([df3['Subject'], df3['Keyword'], df3['Count']], axis = 1) 
    data = dataf.groupby("Subject")["Keyword"].apply(list)
    dic = data.to_dict()

    return(dic)
        

def extract_formations(text):
    """
    Extracts specific portions of the given text and returns
    a well defined dictionnary containing formations' details.
    """
    
    # Empty text
    if len(text) < 1:
        return {}
    
    formations_dict = {}
    formations_list = text.split("\n")
    for formation in formations_list:
        formation_words = formation.split(" ")
        # Formation has at most two words and is one of the banned sentences  
        if any(banned_sentence in formation for banned_sentence in FORMATION_BANNED_SENTS) and len(formation_words) < 4:
            continue
        # Formation has one word
        if len(formation_words) < 3:
            continue
        formations_dict[f"formation_{len(formations_dict) + 1}"] = formation

    return formations_dict


def extract_loisirs(text):

    

    """
    Extracts the field 'loisirs' from Cvs and returns dictionary  

    In: str
    Out: dict
    """
    
    porterStem = nltk.PorterStemmer()

    text = unidecode(text)
    
    
    for k, list_values in LOISIRS_DICT.items():
        list_values_norm = []
        for word in list_values:
            list_values_norm.append(porterStem.stem(unidecode(word).lower()))
        
        LOISIRS_DICT[k]=list_values_norm   
        
    
    matches_extracted = {key:list() for key in LOISIRS_DICT.keys()}
        
    for word in text.split():
        for k, v in LOISIRS_DICT.items():  
            if porterStem.stem(word.lower()) in v:
                matches_extracted[k].append(word)    
    

    return matches_extracted


def extract_experiences(text):
    """
    Extracts specific portions of the given text and returns
    a well defined dictionnary containing experiences' details.
    """
    
    # Empty text
    if len(text) < 1:
        return {}
    
    experiences_dict = {}
    experiences_list = text.split("\n")
    for experience in experiences_list:
        experience_words = experience.split(" ")
        # Experience has at most two words and is one of the banned sentences  
        if any(banned_sentence in experience for banned_sentence in EXPERIENCES_BANNED_SENTS) and len(experience_words) < 4:
            continue
        # Experience has one word
        if len(experience_words) < 3:
            continue
        experiences_dict[f"experience_{len(experiences_dict) + 1}"] = experience
        
    return experiences_dict


def extract_langues(text):
    #TODO: fixme
    text = str(text)
    if len(text) < 1:
        return {}

    d = {}
    text = text.replace("frangais", "francais")
    text = text.replace(".", "")
    
    for word in text.split():
        if word in LANGUES :
            try:
                res = text.split(word, maxsplit=1)[-1].split(maxsplit=1)[0]
                res2 = text.split(res, maxsplit=1)[-1].split(maxsplit=1)[0]
            except IndexError:
                res = ""
                res2 = ""
                text = text + "Exception"
            
            if res in NIVEAUX:     
                d[word] = res
            elif (res2 in NIVEAUX and res not in LANGUES):
                d[word] = res2
            else:
                d[word] = ''

    return d

def extract_infos_personelles(text):
    def extract_emails(text):  
        mail_regex = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                            "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                            "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))
        
        emails = re.findall(mail_regex, text) 

        return [email[0] for email in emails]

    def extract_phone(text):
        phone_regex = re.compile("(\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}|\d{2}[-\.\s]??\d{4}[-\.\s]??\d{4}|\d{1}[-\.\s]??\d{3}[-\.\s]??\d{3}[-\.\s]??\d{3}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{3}[-\.\s]??\d{3}[-\.\s]??\d{3}|\d{3}[-\.\s]??\d{1}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}|\d{3}[-\.\s]??\d{3}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}|\d{2}[-\.\s]??\d{2}[-\.\s]??\d{3}[-\.\s]??\d{3}|\d{4}[-\.\s]??\d{1}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}[-\.\s]??\d{2}|\d{3}[-\.\s]??\d{9})")
        phones = phone_regex.findall(text)
        if len(phones)>0 :     
            return phones[0]
        return ""
    
    def extract_postal_code(text):
        postal_regex = re.compile("([\s]\d{5}[\s])")
        code_postal = postal_regex.findall(text)
        if len(code_postal)>0 :
            return code_postal[0]
        return ""
    

    def extract_age(text):  
        age_regex = re.compile("([\s]\d{2}[\s](ans)|[\s]\d{2}(ans)|[\s]\d{2}(ans)[\s]|(:)\d{2}(ans)[\s]?|[\s]\d{2}[\s](an)[\s]?)")
        ages = age_regex.findall(text)
        if len(ages)>0 :
            return ages[0][0]
        return ""
    
    infos_dict={}
    infos_dict["Numero_tel"] = extract_phone(text)
    infos_dict["Code_postale"] = extract_postal_code(text)
    infos_dict["Age"] = extract_age(text)
    infos_dict["Email"] = extract_emails(text)

    for x in infos_dict.values() :  
        if len(x)>0 :
            if type(x)==list:
                for v in x:
                    text = text.replace(v,"")
            else:
                text = text.replace(x,"")

    infos_dict["Autre"] = text



    return(infos_dict)


def make_json(boxes_dataframe, competences_dataset_path="domain\datasets\external datasets\competences.csv", json_file_name="", log_folder=""):
    """
    From the given boxes_dataframe, creates a well defined JSON object
    containing information parsed from a CV.
    """


    data_by_label_id = merge_text_by_label(boxes_dataframe, "\n")
    
    data = data_by_label_id.copy()
    data["label"] = data_by_label_id.label.map(get_label_name)
    
    #TODO: refactor
    data_dict = {}
    for _, row in data.iterrows():
        label = str(row["label"])
        if label == "Projets":
            data_dict[label] = extract_projects(row["text"])
        elif label == "Competences":
            data_dict[label] = extract_competences(text=row["text"], competences_dataset_path=competences_dataset_path)           
        elif label == "Formations":
            data_dict[label] = extract_formations(row["text"])
        elif label == "Loisirs":
           data_dict[label] = extract_loisirs(row["text"])
        elif label == "Experiences":
            data_dict[label] = extract_experiences(row["text"])
        elif label == "Langues":
            data_dict[label] = extract_langues(row["text"])
        elif label == "Infos_personnelles":
            data_dict[label] = extract_infos_personelles(row["text"])
        else:
            data_dict[label] = row["text"]

    json_dict = JSON_OBJ
    for label in data_dict:
        json_dict[label] = data_dict[label]

    if len(log_folder)>0:
        log_make_json_files(data, log_folder)

    return json_dict
    

def log_make_json_files(merged_data, log_folder):
    """
    """

    merged_data.to_csv(log_folder + "\merged_data.csv", index=False)