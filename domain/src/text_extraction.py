import sys
import cv2
import pytesseract
from PIL import Image
import pandas as pd
import numpy as np
import logging
from src.utils import save_dict


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


# For windows users
if sys.platform == 'win32':
    pytesseract.pytesseract.tesseract_cmd = (
        r"C:\Program Files\Tesseract-OCR\tesseract"
    )


def text_extraction(image, regions, log_folder=""):
    """
    Extracts text from all given regions and
    returns a dict containing coordinates and text.
    """
    
    boxes = {}
    logging.info(f'Performing text extraction on image...')
    for key, box in regions.items():
        logging.info(f'Extracting text from {key}...')
        text = extract_text(
            image,
            box["xmin"], box["xmax"], 
            box["ymin"], box["ymax"])
        boxes[key] = {
                "xmin": box["xmin"],
                "ymin": box["ymin"],
                "xmax": box["xmax"],
                "ymax": box["ymax"],
                "text": text
            }

    if len(log_folder) > 0:
        log_text_extraction_files(boxes, log_folder)

    return boxes


def extract_text(image, x_min, x_max, y_min, y_max):
    """
    From a PIL image object, extracts and returns the text
    contained inside the box delimited by the given coordinates.
    """

    ROI = image[y_min:y_max, x_min:x_max]
    text = pytesseract.image_to_string(ROI)

    return text


def get_text(row, images_path):
    """
    Using row's data, calls a function to perform OCR for text
    extraction and return the extracted text.
    """
    print(row)
    factor = 10
    x_min, x_max, y_min, y_max = row["xmin"]*factor, row["xmax"] * \
        factor, row["ymin"]*factor, row["ymax"]*factor
    image = Image.open(images_path + row["filename"])

    text = extract_text(image, x_min, x_max, y_min, y_max)

    return text


def add_text_to_csv(csv_file_path, images_path):
    """
    Loops over all images files mentionned in a csv file,
    for each coordinates extracts text contained in a image
    box.
    """

    data = pd.read_csv(csv_file_path)
    text = data.apply(get_text, images_path=images_path, axis=1)
    data["text"] = text

    data.to_csv(csv_file_path[:-4] + "_updated.csv", index=False)


def log_text_extraction_files(boxes, log_folder):
    """
    """

    save_dict(boxes, log_folder + "/boxes.json")


if __name__ == "__main__":
    # execute only if run as a script
    with time_usage("Extraction du texte d'une seule box avec Tesseract"):
        add_text_to_csv(csv_file_path="datasets/test.csv",
                        images_path="datasets/")
