import logging
import pickle
import pandas as pd

from src.train import classification_preprocessing, feature_engineering
from src.preprocessing import clean_boxes_dataset


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def apply_classification(boxes,
                         model_path="domain/models/best_classification_model",
                         tfidf_model_path="domain/models/tfidfconverter",
                         svd_model_path="domain/models/svd",
                         log_folder=""):
    """Classifies the content of each detected box and returns a 
    pd.DataFrame containing box's coordinates, its text and classification
    result (label_id).  
    """

    # Converting input (dict) into a pd.DataFrame
    data = pd.DataFrame(
        {"xmin": [], "ymin": [], "xmax": [], "ymax": [], "text": []})
    for _, region in boxes.items():
        row = {
            "xmin": region["xmin"],
            "xmax": region["xmax"],
            "ymin": region["ymin"],
            "ymax": region["ymax"],
            "text": region["text"],
        }
        data = data.append(row, ignore_index=True)

    logging.info(f'Cleaning boxes...')
    cleaned_data = clean_boxes_dataset(data)

    logging.info(f'Applying stemming on text...')
    preprocessed_data, preprocessed_dataframe = classification_preprocessing(
        cleaned_data)

    logging.info(f'Applying feature engineering process...')
    X = feature_engineering(preprocessed_data, 
        tfidf_model_path=tfidf_model_path, svd_model_path=svd_model_path)


    logging.info(f'Predicting boxes classes...')
    with open(model_path, 'rb') as model_binaries:
        model = pickle.load(model_binaries)
    labels = model.predict(X)
    preprocessed_dataframe["label"] = labels

    if len(log_folder) > 0:
        logging.info(f'\tSaving intermediate files...')
        log_apply_classification_files(
            cleaned_data, preprocessed_dataframe, log_folder)

    return preprocessed_dataframe


def log_apply_classification_files(cleaned_data, 
                                   classif_preprocessed_data,
                                   log_folder):
    """Saves the intermediate generated files of the classification step into
    a folder.
    """

    cleaned_data.to_csv(log_folder+"/cleaned_data.csv", index=False)

    del classif_preprocessed_data["index"]
    classif_preprocessed_data.to_csv(
        log_folder+"/classif_preprocessed_data.csv", index=False)
