import cv2
import pandas as pd
from src.utils import read_image, save_image
from src.postprocessing import get_label_name
from src.region_detection import region_detection
from src.text_extraction import text_extraction
from src.boxes_classification import apply_classification
import pickle


def show_bounding_boxes(image, boxes, show=False, save=True, image_name="output/show_bounding_boxes", extra_text=""):
    """
    Draws bounding bounding boxes on the given image,
    and opens a new windows containing the result. 
    """

    for _, region in boxes.items():
        x_min = int(region["xmin"])
        x_max = int(region["xmax"])
        y_min = int(region["ymin"])
        y_max = int(region["ymax"])
        
        image = cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (36,255,12), 2)
        image = cv2.putText(image, extra_text, (20, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        #image = cv2.rectangle(image, start_point, end_point, color, thickness) 
    if save:
        cv2.imwrite(f"{image_name}.png", image)
    if show:
        cv2.imshow('image', image)


def show_classification(image, boxes_dataframe, extra_text=""):
    """
    """

    for _, box in boxes_dataframe.iterrows():
        x_min = int(box["xmin"])
        x_max = int(box["xmax"])
        y_min = int(box["ymin"])
        y_max = int(box["ymax"])
        label = box["label"]

        image = cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (36,255,12), 2)
        image = cv2.putText(image, str(label), (x_min-10, y_min-10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (36,255,12), 2)
        image = cv2.putText(image, extra_text, (20, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    
    return image


def save_classification_results(image, log_folder, dilatation_itr):

    boxes = pd.read_csv(log_folder +"/classif_preprocessed_data.csv")
    extra_text = f"dilatation_iterations_{dilatation_itr}__nbre_boxes_{len(boxes)}"
    result_image = show_classification(image, boxes, extra_text)
    save_image(log_folder + "/result_image.jpg", result_image)


def draw_labeled_boxes(dilatation_itr, image_path="", image=0, output_path="output/exemple.jpg", show=False, extra_text=""):
    """
    """


    if image==0:
        image = read_image(image_path)
    regions = region_detection(image, iterations=dilatation_itr)
    boxes = text_extraction(image, regions)
    labeled_boxes = apply_classification(boxes)
    labeled_boxes.label = labeled_boxes.label.map(get_label_name)
    image = show_classification(image, labeled_boxes, extra_text=extra_text + f"_num_labeled_boxes_{str(len(labeled_boxes))}")
    save_image(output_path, image)
    if show:
        cv2.imshow(output_path, image)
        cv2.waitKey(0)