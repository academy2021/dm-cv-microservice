from src.utils import read_image, time_usage, save_image
from src.visualize import draw_labeled_boxes
from src.region_detection import region_detection
from src.text_extraction import text_extraction
from src.boxes_classification import apply_classification
from src.postprocessing import get_label_name


def dilatation_benchmark(image_path, output_folder_path="output/dilatation_benchmark"):
    """Creates a folder containing several output images, results of
    applying different dilattaiton coefficients on an input image.
    For each image, it draws detected boxes and labels the image (at
    the top left) with a string containing information about the number
    of boxes and the used dilatation coefficient.
    """

    for dilatation_itr in range(2, 8):
        draw_labeled_boxes(
            image_path, dilatation_itr,
            output_path=output_folder_path +
            f"/dilatation_{dilatation_itr}.jpg",
            extra_text=f"_dilatation_{dilatation_itr}")
