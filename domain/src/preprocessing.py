import pandas as pd
import logging

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

pd.options.mode.chained_assignment = None

def clean_boxes_dataset(data):
    """Cleans boxes dataset."""

    # Remove lines with empty text
    cleaned_data = data.dropna(subset=["text"])

    # Remove special caracters
    cleaned_data["text"] = cleaned_data["text"].map(lambda x: clean_text(x))

    # Remove text with length smaller that a constant
    cleaned_data = cleaned_data[cleaned_data["text"].map(len) > 3]

    return cleaned_data


def clean_text(text):
    """Performs classical cleaning preprocessing on
    given text.
    """


    # Replace punctuation characters with spaces
    filters='!"$%&()*-/;<=>?[\\]^`{|}~\t\n'
    translate_dict = dict((c, " ") for c in filters)
    translate_map = str.maketrans(translate_dict)
    text = text.translate(translate_map)

    # Convert text to lowercase
    text = text.strip().lower()

    return text