from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
import logging
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import pickle
import numpy as np
import pandas as pd
import re
import nltk
from sklearn.datasets import load_files
nltk.download('stopwords')
nltk.download('wordnet')
from xgboost import XGBClassifier
from sklearn.decomposition import TruncatedSVD

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
# cv_name,box_name,xmin,ymin,xmax,ymax,text,label


def classification_preprocessing(X):
    """
    """

    documents = []
    stemmer = WordNetLemmatizer()
    
    X = X.reset_index()

    for sen in range(0, len(X)):
        # Remove single characters from the start
        document = re.sub(r'\^[a-zA-Z]\s+', ' ', X.text[sen])

        # Substituting multiple spaces with single space
        document = re.sub(r'\s+', ' ', document, flags=re.I)

        # Lemmatization
        document = document.split()

        document = [stemmer.lemmatize(word) for word in document]
        document = ' '.join(document)

        documents.append(document)

    cleaned_data = X.copy()
    cleaned_data["text"] = documents

    return documents, cleaned_data


def feature_engineering(
    documents, tfidf_model_path="domain/models/tfidfconverter",
    svd_model_path="domain/models/svd", retrain=False):
    """
    """

    if retrain:
        tfidfconverter = TfidfVectorizer(min_df=3, max_features=None, 
            strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
            ngram_range=(1, 3), use_idf=1, smooth_idf=1, sublinear_tf=1)

        logging.info(f'\tRetraining TfidfVectorizer...')
        X = tfidfconverter.fit_transform(documents).toarray()

        logging.info(f'\tSaving TfidfVectorizer model...')
        with open(tfidf_model_path, 'wb') as picklefile:
            pickle.dump(tfidfconverter, picklefile)
        
        logging.info(f'\tRetraining SVD...')
        svd = TruncatedSVD(n_components=100, random_state=0)
        svd.fit(X)
        X = svd.transform(X)

        logging.info(f'\tSaving SVD model...')
        with open(svd_model_path, 'wb') as picklefile:
            pickle.dump(svd, picklefile)

    else:
        logging.info(f'\tLoading TfidfVectorizer model...')
        with open(tfidf_model_path, 'rb') as model_binaries:
            tfidfconverter = pickle.load(model_binaries)
        X = tfidfconverter.transform(documents).toarray()

        logging.info(f'\tLoading SVD model...')
        with open(svd_model_path, 'rb') as model_binaries:
            svd_model = pickle.load(model_binaries)
        X = svd_model.transform(X)

    return X


def train_baseline_model(dataset_path, verbose=True):
    """
    """

    data = pd.read_csv(dataset_path)
    X, y = data.drop(["cv_name", "box_name", "label"], axis=1), data.label

    logging.info(f'Stemming...')
    documents, _ = classification_preprocessing(X)

    logging.info("Feature engineering...")
    X = feature_engineering(documents)

    logging.info(f'Training...')
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=0)

    classifier = RandomForestClassifier(n_estimators=1000, random_state=0)
    classifier.fit(X_train, y_train)

    y_pred = classifier.predict(X_test)

    logging.info(f'Evaluation...')
    if verbose:
        print(confusion_matrix(y_test, y_pred))
        print(classification_report(y_test, y_pred))
        print(accuracy_score(y_test, y_pred))

    logging.info(f'Saving model...')
    with open('models/baseline_model', 'wb') as picklefile:
        pickle.dump(classifier, picklefile)


def train_best_model(dataset_path, verbose=True):
    """
    """

    data = pd.read_csv(dataset_path)
    X, y = data.drop(["cv_name", "box_name", "label"], axis=1), data.label

    logging.info(f'Stemming...')
    documents, _ = classification_preprocessing(X)

    logging.info("Feature engineering...")
    X = feature_engineering(documents)

    logging.info(f'Training...')
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=0)

    classifier = XGBClassifier()
    classifier.fit(X_train, y_train)

    y_pred = classifier.predict(X_test)

    logging.info(f'Evaluation...')
    if verbose:
        print(confusion_matrix(y_test, y_pred))
        print(classification_report(y_test, y_pred))
        print(accuracy_score(y_test, y_pred))

    logging.info(f'Saving model...')
    with open('domain/models/best_classification_model', 'wb') as picklefile:
        pickle.dump(classifier, picklefile)

# train_baseline_model("datasets/classification_dataset.csv")
#train_best_model("datasets/classification_dataset_2.csv")