import fitz
import glob
import logging

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def pdf_to_image(pdf_file_path, zoom_factor=3):
    """
    Converts a pdf file into a list of np.array 
    images and  saves the result using the same name. If the pdf file 
    has multiple page, a number is added to images'
    names starting from 1 (as the second page).
    """


#TODO: refactor

def pdf_to_jpg(pdf_file_path, zoom_factor=3):
    """
    Converts a pdf file into a jpg image and saves
    the result using the same name. If the pdf file 
    has multiple page, a number is added to images'
    names starting from 1 (as the second page).
    """

    doc = fitz.open(pdf_file_path)
    page = doc.loadPage(0)
    mat = fitz.Matrix(zoom_factor, zoom_factor)
    pix = page.getPixmap(matrix=mat)
    print(pix)
    print(type(pix))
    print(pix.this)
    output = pdf_file_path[:-4] + ".jpg"
    pix.writePNG(output)
    for i in range(1, doc.pageCount):
        page = doc.loadPage(i)
        pix = page.getPixmap(matrix=mat)
        output = pdf_file_path[:-4] + f"{i}.jpg"
        pix.writePNG(output)

"""
for file in glob.glob("datasets/pdf/*.pdf"):
    logging.info(f'CV name: {file}')
    pdf_to_jpg(file, zoom_factor=3)
    break
"""