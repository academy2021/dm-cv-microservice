import cv2
import logging
from contextlib import contextmanager
import time
import json
import numpy as np


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def read_image(image_path):
    """
    Reads an image from path.
    """

    logging.info(f'Rading image from path: {image_path}')
    image = cv2.imread(image_path)

    return image


def read_image_from_buffer(image_bytes, zoom=1):
    """
    Reads an image from path and returns matrix.
    """

    #logging.info(f'Rading image from buffer: {image_bytes}')
    image = cv2.imdecode(np.frombuffer(image_bytes, np.uint8), -1)
    #Zooming
    if zoom > 1:
        height, width = image.shape[:2]
        image = cv2.resize(image, (zoom*width, zoom*height), interpolation = cv2.INTER_CUBIC)

    return image



def save_image(image_name, image):
    """
    """

    cv2.imwrite(image_name, image)


def save_dict(json_dict: dict, json_file_path):
    """
    """

    with open(json_file_path, 'w') as fp:
        json.dump(json_dict, fp)


def save_txt(text, txt_path):
    """
    """

    with open(txt_path, "w") as text_file:
        text_file.write(text)
        text_file.close()


def read_json(json_file_path):
    """
    """

    with open(json_file_path) as json_file:
        data = json.load(json_file)

    return data


def build_log_txt(
        buffer_image, dilatation_iterations, output_name, with_merge, log_folder,
        image, regions, boxes, labeled_boxes, json_res, seconds_elapsed):
    """
    Builds a string containing interesting information about the execution
    of the pdf_to_json function and its results.
    """

    content = f"""buffer_image = {buffer_image}
dilatation_iterations = {dilatation_iterations}
output_name = {output_name}
with_merge = {str(with_merge)}
regions_number = {len(regions)}
image_dimensions = {str(image.shape)}
boxes_number = {len(boxes)}
labeled_boxes_number = {len(labeled_boxes)}
json_res_len = {len(json_res)}
seconds_elapsed = {str(seconds_elapsed)}
    """

    return content


@contextmanager
def time_usage(name=""):
    """
    Logger le temps passé lors de l'execution d'une partie du code.
    """

    start = time.time()
    yield
    end = time.time()
    elapsed_seconds = float("%.4f" % (end - start))
    logging.info('%s: secondes écoulées: %s', name, elapsed_seconds)

    return elapsed_seconds
