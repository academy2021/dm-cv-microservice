import os,sys,inspect
from etl.get_image import get_image
from domain.main import pdf_to_json

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

def doc_to_json(document_path):

    document_buffer = open(document_path, "rb").read()
    document_name = document_path.split("\\")[-1][:-4]
    input_buffer = get_image(document_buffer)
    doc_json = pdf_to_json(
        dilatation_iterations=5,
        buffer_image=input_buffer,
        output_name=f"output/{document_name}/{document_name}.json")

    return doc_json

def fs_doc_to_json(folder_path):

    fs_json = []
    for doc_path in os.listdir(directory):
        doc_json = doc_to_json(doc_path)
        fs_json.append(doc_json)
    return fs_json
    
doc_to_json("domain\datasets\pdf\ADNANE%20Maryam.pdf")
doc_to_json("domain\datasets\dataset zoom 3\ADNANE_20Maryam.jpg")
