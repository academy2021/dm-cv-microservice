import sys

import filetype

from etl.pdf_to_jpg import pdf_to_jpg
from etl.doc_to_jpg import doc_to_jpg


def get_image(input_buffer):

    input_buffer_type = filetype.guess(input_buffer).mime.split("/")[1]
    if input_buffer_type in ["jpg", "jpeg", "png"]:
        return input_buffer
    elif input_buffer_type == "pdf":
        output_buffer = pdf_to_jpg(input_buffer)
        return output_buffer
    elif input_buffer_type in ["zip", "doc", "docx"]:
        output_buffer = doc_to_jpg(input_buffer)
        return output_buffer

    return
