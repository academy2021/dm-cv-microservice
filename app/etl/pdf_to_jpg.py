import fitz
import os

def pdf_to_jpg(input_buffer_pdf):
    
    doc = fitz.open("pdf", input_buffer_pdf)
    page = doc.loadPage(0)
    mat = fitz.Matrix(3, 3)
    pix = page.getPixmap(matrix=mat)
    pix.writePNG("temp_file.jpg")
    buffer = open("temp_file.jpg", "rb").read()
    os.remove("temp_file.jpg")

    return buffer
