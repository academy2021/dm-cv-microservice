import os
from docx2pdf import convert
from etl.pdf_to_jpg import pdf_to_jpg


def doc_to_pdf(doc_buffer):
    cwd = os.getcwd()
    pdf_path = cwd + "/temp_file_1.pdf"
    doc_path = 'temp_file.docx'

    with open(doc_path, 'wb') as output_file:
        output_file.write(doc_buffer)

    convert(doc_path, pdf_path)

    pdf_buffer = open(pdf_path, "rb").read()

    os.remove(pdf_path)
    os.remove(doc_path)

    return pdf_buffer

def doc_to_jpg(doc_buffer):

    pdf_buffer = doc_to_pdf(doc_buffer)

    image_buffer = pdf_to_jpg(pdf_buffer)

    os.remove(pdf_path)
    os.remove(doc_path)

    return image_buffer
