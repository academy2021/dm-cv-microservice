import os
import sys
import inspect

from flask import Flask, request, jsonify
from flask_cors import CORS
from etl.get_image import get_image

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
from domain.main import pdf_to_json

app = Flask(__name__)

CORS(app)
@app.route("/api/parsecv", methods=["POST"])
def parse_cv():

    document_buffer = request.files['document'].read()
    document_name = request.files['document'].filename
    input_buffer = get_image(document_buffer)
    output = pdf_to_json(
        dilatation_iterations=5,
        with_merge=True,
        buffer_image=input_buffer,
        output_name=f"output/{document_name}/{document_name}.json"
    )
    return jsonify(output)


@app.route("/api/count", methods=["GET"])
def count():
    return get_count()

@app.route("/api/ping", methods=["GET"])
def ping():
    """ ping the api, answers 'ok' """
    return "ok"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
