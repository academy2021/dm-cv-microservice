from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan


INDEX_NAME = "cv_parsing_index"
ES = Elasticsearch(
        ["http://172.16.3.168:9200"],
        sniff_on_start=True,
        sniff_on_connection_fail=True,
        sniffer_timeout=60
        )
if not ES.indices.exists(index=INDEX_NAME):
    ES.indices.create(index=INDEX_NAME)


def save_result(document):
    """
    """
    ES.index(index=INDEX_NAME, doc_type="cv", body=document)


def get_count():
    """
    Returns the number of documents saved in INDEX.
    """

    es_response = scan(
        ES,    
        index=INDEX_NAME,
        doc_type='cv',
        query={"query": { "match_all" : {}}}
        )

    return str(len([1 for _ in es_response]))