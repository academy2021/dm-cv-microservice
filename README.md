# Document Management CV Microservice

Document Management CV Microservice, is the backend microservice of the Document Management project, that uses AI to parse CVs and to extract useful information. It accepts documents with extensions .doc, .jpg or .pdf.
